#!/bin/bash

STUPID_DIR=$(pwd)

function installRoutine {
	if [ "$2" = '1' ]; then
		echo "  <$(date +'%H:%M:%S')> Installing $1 ..."
	else
		echo "<$(date +'%H:%M:%S')> Installing $1 ..."
	fi

	if grep -q "$1" 'installed.stupid-log'; then
		echo    "  <W> Package has already been installed! It will be reinstalled!"
		read -p "      Do you want to continue? [y/N]: " confirm && ! [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] && exit

		removeRoutine $1 '1'
	elif grep -q "$name" 'installed.stupid-log'; then
		otherPkg=$(grep "$name--*" 'installed.stupid-log')
		echo    "  <W> Another version of the package has been found! $otherPkg will be removed and $1 will be installed!"
		read -p "      Do you want to continue? [y/N]: " confirm && ! [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] && exit

		removeRoutine $otherPkg '1'
	fi

	mkdir -p "/tmp/stupid/$name" && cd "/tmp/stupid/$name"; touch "$1.install-log"
	stupidInstall >"$1.install-log" 2>&1 || (installFail $1 $2 && exit)

	echo "FINISHED" >> "$1.install-log"; echo "$1" >> "$STUPID_DIR/installed.stupid-log"
	if [ "$2" = '1' ]; then
		echo "    <$(date +'%H:%M:%S')> Installed $1!"
	else
		echo "  <$(date +'%H:%M:%S')> Installed $1!"
	fi
}

function installFail {
	if [ "$2" = '1' ]; then
		echo "    <E> Could not install! Log:"
	else
		echo "  <E> Could not install! Log:"
	fi
	cat "/tmp/stupid/$name/$1.install-log"
}

function removeRoutine {
	if [ "$2" = '1' ]; then
		echo "  <$(date +'%H:%M:%S')> Removing $1 ..."
	else
		echo "<$(date +'%H:%M:%S')> Removing $1 ..."
	fi

	if ! grep -q "$1" 'installed.stupid-log'; then
		echo "  <W> Package isn't installed!" && exit
	fi

	mkdir -p "/tmp/stupid/$name" && cd "/tmp/stupid/$name"; touch "$1.remove-log"
	stupidRemove >"$1.remove-log" 2>&1 || (removeFail $1 $2 && exit)

	grep -qv "$1" "$STUPID_DIR/installed.stupid-log" > "$STUPID_DIR/installed.stupid-log"
	if [ "$2" = '1' ]; then
		echo "    <$(date +'%H:%M:%S')> Removed $1!"
	else
		echo "  <$(date +'%H:%M:%S')> Removed $1!"
	fi
}

function removeFail {
	if [ "$2" = '1' ]; then
		echo "    <E> Could not remove! Log:"
	else
		echo "  <E> Could not remove! Log:"
	fi
	cat "/tmp/stupid/$name/$1.remove-log"
}

function handlePackages {
	routine=$1
	shift; shift

	while [ ! -z $1 ]; do
		# Package names can either be "PACKAGENAME" or "PACKAGENAME--VERSION"
		# If it's "PACKAGENAME" we only want the last occurence, since alphanumerically this should be the latest version
		pkg=$(find . -type f -name "$1--*.stupid" | tail -n 1 || grep -q '--' && find . -type f -name "$1.stupid")
		if [ -z $pkg ]; then
			echo "<E> No package file for $1!"; shift; continue
		fi

		# Ran in subshell so the source-ing doesn't persist between routine functions of multiple packages
		(
			source "$pkg"
			if [ "$1--$version" != "$name--$version" ] && [ "$1" != "$name--$version" ]; then
				echo "<E> Package file name and package name and/or version do not match for $1!"; exit
			fi
			if [ $(type -t stupidInstall) != "function" ]; then
				echo "<E> No install function found for $1!"; exit
			fi
			if [ "$(type -t stupidRemove)" != "function" ]; then
				echo "<E> No remove function found for $1!"; exit
			fi

			$routine "$name--$version"
		)
		shift
	done
}

function updateAll {
	while read p; do
		pkg=$(find . -type f -name "$(egrep -o '^[^--]+' <<< $p)--*.stupid" | tail -n 1)
		if [ "$p.stupid" != "$pkg" ] && ! [ -z "$p" ]; then
			echo "<I> Updating $p ..."
			(
				source "$pkg"
				removeRoutine $p
				installRoutine "$name--$version"
			)
		fi
	done < 'installed.stupid-log'
}

while [ ! -z $1 ]; do
	case $1 in
		-i|install)
			handlePackages installRoutine $@ ; exit
			;;
		-r|remove)
			handlePackages removeRoutine $@ ; exit
			;;
		-u|update)
			updateAll ; exit
			;;
		-q|query)
			echo 'Not implemented yet!' ; exit
			;;
		*)
			echo "<Error> Invalid command \"$1\"!" && exit 1
			;;
	esac
	shift
done

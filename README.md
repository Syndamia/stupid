# stupid

stupid is a dead simple "packet" manager, designed more as manual automation than anything else. In brief, each package file has two bash functions, one for installing and one for removing, which are ran when installing or removing a package.

**Do NOT use simple with other people's package files!** As mentioned, these functions are ran as-is, as the root user. This could very easily do something malicious to your system.

**simple must not be used as a replacement to any other package manager!** It is fairly obvious why, but it doesn't (and won't) have many lifesaving features most package managers have. Also, package files cannot be trusted.

### Why would I want to use this?

In an ideal world, you shouldn't (hence the name), you should configure a local repository with packages for your actual package manager.

But, that isn't always straight forward and could consume too much time. Also, it is not impossible your packet manager just doesn't support the exact way you want to install a package.

stupid is a compromise, it's worse than just configuring your actual package manager, but it is a bit better than arbitrarily installing a package and then forgetting how to remove it a month later.

## Dependencies

stupid is nothing more than a bash script, so all of it's requirements are certain basic commands, which should mostly come with your distribution.

`bash`, `echo`, `mkdir`, `cd`, `tail`, `grep`, `find`, `cat`, `read`

### How to install

TBD

## How to use

TBD

## Creating your own package files

**All** package files are named in the format `PACKAGENAME--VERSION.stupid`.

TBC

## Feature status

- [x] Install
- [x] Remove
- [ ] Search
- [ ] Update
